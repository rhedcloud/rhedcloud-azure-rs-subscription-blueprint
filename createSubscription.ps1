# Author: Joey Brakefield
# Date: 09/02/20
# Name: Create a new subscription under an Azure Enrollment

# Steps
# 1. One-time only (subscription where we can run tests as part of a pipeline) -- Permanent as part of RHEDcloud master deploy
# 2. Details of the RHEDcloud Azure RS Sub Blueprint
# 3. Write a python test (on commit of the RS Sub Blueprint) that's part of the Bitbucket pipeline 
# 4. Bitbucket pipeline .yml to run the tests above

# PowerShell/REST Steps to create an Azure subscription

# 1. <MANUAL STEP, Run once> Get the Azure Enrollment ID - Log into the Azure PowerShell/CLI with an Azure Active Directory account that has been assigned "Account Owner" in the EA portal
connect-azaccount # THIS MUST BE INTERACTIVE LOGIN -- COMMENT THIS WHEN AUTOMATING PIPELINES OR CI/CD'ING YOU STUFF
$enrollID=$(Get-AzEnrollmentAccount).ObjectId # <---- This output is what is needed in step #3 to create the subscription. Need to put as pipeline secret variable

# 2. <MANUAL STEP, Run once> Grant access to your service principal to create subscriptions with that newly found $enrollID. NOTE: put the service account into a **SECURITY GROUP** and then grant the Role of "Owner" to that group.
$grpOidSvcPrinc="7d8328e1-be20-473d-8ef2-cfdb8e712a63"
New-AzRoleAssignment -RoleDefinitionName Owner -ObjectId $grpOidSvcPrinc -Scope /providers/Microsoft.Billing/enrollmentAccounts/$enrollID

# 3. Create the new sub
$subName="Testingsub-"+$(get-date -Format FileDateTime)
$RHEDcloudAdminOID = "a8319c78-8c01-40ef-a0e5-275c09367c18" # This is the Central Admin group for RHEDcloud IT admins

$newSub=New-AzSubscription -OfferType MS-AZR-0017P -Name $subName -EnrollmentAccountObjectId $enrollID -OwnerObjectId $RHEDcloudAdminOID
Select-AzSubscription -SubscriptionObject $newSub

# 4. Enable Resource Providers for the new subscription
## $newSub is the object on which we are enabling the "macro-level" Resource Providers in Azure


# Per Subscription
Register-AzResourceProvider -ProviderNamespace Microsoft.Compute        # EC2
Register-AzResourceProvider -ProviderNamespace Microsoft.Storage        # S3, Data Lake
Register-AzResourceProvider -ProviderNamespace Microsoft.Network        # VPC
Register-AzResourceProvider -ProviderNamespace Microsoft.Sql            # RDBMS
Register-AzResourceProvider -ProviderNamespace Microsoft.DocumentDB     # DynamoDB
# Register-AzResourceProvider -ProviderNamespace Microsoft.Batch        # Batch leaving this commented to test Azure Blueprints at the Management Group level


# Management at the RHEDcloud Admin Sub
# Microsoft.KeyVault  # Secrets Manager 

# TODO: Can a policy be defined to disallow root level subscription creation?
# TODO: Define location policy and definte, publish to mgmt group. Assign to a demo subscription. Try to create a resource (resource group) in that attempts to violate the location policy.